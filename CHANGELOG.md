# Changelogs

## Important changes

### V 2.1.4-draft3 (2022-12-27)

Fix compatibility with old compilers (ex. GCC 5.4.0)

### V 2.1.4-draft2 (2022-12-18)

Virtual-ize shared reset function

### V 2.1.4-draft1 (2022-12-16)

Split CMakeLists per each directories

### V 2.1.4-draft0 (2022-12-12)

Constant-ize, Inline-ize functions and arguments
Add AYPSG, DCSG, Namco C140/C219, Nanao GA20, SM8521 core (WIP)
Split SCC mapper and sound core emulation
Add per-stream update function on some cores

### V 2.1.3 (2022-10-22)

Fix ES5504, ES5505, ES5506 per-voice preview functions

### V 2.1.2 (2022-10-21)

Split utilities and core framework header
Use static constexprs for global constants
Fix initialization
Fix CMake

### V 2.1.1 (2022-10-20)

Add C++11 detection for CMake

### V 2.1.0 (2022-09-08)

Move source folder into vgsound_emu folder
CMake support
Move each readmes into README.md each folders

## Details

See [here](https://gitlab.com/cam900/vgsound_emu/-/commits/main).

### Previous changelogs

See [here](https://github.com/cam900/vgsound_emu/commits/main).
