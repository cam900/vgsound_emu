# OKI MSM5205/MSM6585

## Summary

- 1 voice Dialogic ADPCM decoder
  - 3 bit (MSM5205 only) or 4 bit input
  - 10 bit output (MSM5205) or 12 bit output with built-in LPF (MSM6585)
- Clock divider via Sn pin (n = 0, 1)

## Source code

- msm5205.hpp: Base header
  - msm5205.cpp: Source emulation core

### Dependencies

- vox.hpp: Dialogic ADPCM decoder header
  - vox.cpp: Dialogic ADPCM decoder source

## TODO

- implement built-in LPF in MSM6585

## Description

It is 1 channel ADPCM decoder chip from OKI semiconductor. It was just nothing else ADPCM decoder with internal timer.

It was 2 variants: MSM5205, MSM6585
MSM5205 supports "slave mode" to allow external clock divider and decoding ADPCM data from Host processor.
MSM6585 has no "slave mode" and 3 bit ADPCM mode but allows higher sample rate and features 12 bit output with built-in LPF.

## Frequency calculation

```
 MSM5205:
  Frequency = Input clock / 96 or 64 or 48 (dependent by S0, S1 pin)
 MSM6585:
  Frequency = Input clock / 160 or 80 or 40 or 20 (dependent by S0, S1 pin)
```
