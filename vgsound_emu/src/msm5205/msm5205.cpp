/*
	License: Zlib
	see https://gitlab.com/cam900/vgsound_emu/-/blob/main/LICENSE for more details

	Copyright holder(s): cam900
	OKI MSM5205 emulation core
*/

#include "msm5205.hpp"

namespace vgsound_emu
{
	s32 msm5205_core::tick()
	{
		if (divider() != 0)
		{
			set_vck_counter(vck_counter() + 1);
			if (vck_counter() >= (divider() << 4))
			{
				set_vck(!vck());
			}
		}
		if ((!vck()) && (counter() < 8))
		{
			if (counter() == 0)
			{
				if ((!reset_pin()) && reset_temp())
				{
					set_reset_temp(0);
				}
			}
			set_counter(counter() + 1);

			if (counter() == 2)	 // 5.2ms
			{
				set_output(output_temp());
			}
			else if (counter() == 6)  // 15.6ms
			{
				if (!reset_temp())
				{
					voice().tick(m_4b_3b ? input() : (input() & ~1));
				}
			}
		}
		else if ((counter() <= 0))
		{
			set_output_temp(reset_temp() ? 0 : voice().out());
			if (reset_pin() && (!reset_temp()))
			{
				set_reset_temp(1);
				voice().reset();
			}
			set_counter(counter() + 1);
		}
		return output() >> 2;  // 10 bit
	}

	s32 msm6585_core::tick()
	{
		if (divider() != 0)
		{
			set_vck_counter(vck_counter() + 1);
			if (vck() && m_first_sample && (vck_counter() == 8))  // VCK/8
			{
				if (!reset_temp())
				{
					voice().tick(input());
					m_first_sample = false;
				}
			}
			if (vck_counter() >= (divider() * 20))
			{
				set_vck(!vck());
			}
		}
		if (vck() && (counter() < 2))
		{
			if (counter() == 0)
			{
				set_output(output_temp());
				set_output_temp(reset_temp() ? 0 : voice().out());
			}
			set_counter(counter() + 1);
			if ((!m_first_sample) && (counter() == 2))	// 3ms
			{
				if (!reset_temp())
				{
					voice().tick(input());
				}
			}
		}
		else if ((!vck()) && (counter() <= 0))
		{
			if (counter() == 0)
			{
				if ((!reset_pin()) && reset_temp())
				{
					set_reset_temp(0);
				}
				if (reset_pin() && (!reset_temp()))
				{
					set_reset_temp(1);
					voice().reset();
					m_first_sample = true;
				}
			}
			set_counter(counter() + 1);
		}
		return output();  // 12 bit
	}

	void msm5205_core::reset()
	{
		m_voice.reset();
		m_reset		  = 1;
		m_reset_temp  = 1;
		m_counter	  = 0;
		m_vck_counter = 0;
		m_out		  = 0;
		m_out_temp	  = 0;
	}

	void msm6585_core::reset()
	{
		msm5205_core::reset();
		m_first_sample = true;
	}

	void msm5205_core::voice_t::tick(u8 input)
	{
		decode(bitfield(input, 0, 4));
		m_out = step();
	}

	void msm5205_core::voice_t::reset()
	{
		vox_decoder_t::reset();
		m_out = 0;
	}

	// accessors
}  // namespace vgsound_emu