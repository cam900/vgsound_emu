/*
	License: Zlib
	see https://gitlab.com/cam900/vgsound_emu/-/blob/main/LICENSE for more details

	Copyright holder(s): cam900
	OKI MSM5205 emulation core
*/

#ifndef _VGSOUND_EMU_SRC_MSM6295_HPP
#define _VGSOUND_EMU_SRC_MSM6295_HPP

#pragma once

#include "../core/core.hpp"
#include "../core/vox/vox.hpp"

namespace vgsound_emu
{
	class msm5205_intf : public vgsound_emu_core
	{
		public:
			msm5205_intf()
				: vgsound_emu_core("msm5205_intf")
			{
			}

			// NE pin is executing voice number, and used for per-voice sample bank.
			virtual void vck_state(bool vck) {}
	};

	class msm5205_core : public vox_core
	{
			friend class msm5205_intf;	// common memory interface

		private:
			// internal divider table (* 16)
			const u8 m_msm5205_divider_table[4] = {3, 4, 6, 0 /* manual upload mode */};

			// msm5205 voice classes
			class voice_t : public vox_decoder_t
			{
				public:
					// constructor
					voice_t(msm5205_core &host)
						: vox_decoder_t("msm5205_voice", host, true)
						, m_host(host)
						, m_out(0)
						, m_mute(false)
					{
					}

					// internal state
					virtual void reset() override;
					void tick(u8 input);

					// Setters
					inline void set_mute(const bool mute) { m_mute = mute; }

					// Getters
					inline s32 out() const { return m_mute ? 0 : m_out; }

				private:
					// accessors, getters, setters
					// registers
					msm5205_core &m_host;  // host core
					s32 m_out = 0;		   // output
					// for preview only
					bool m_mute = false;  // mute flag
			};

		public:
			// constructor
			msm5205_core(msm5205_intf &intf)
				: msm5205_core("msm5205", intf)
			{
			}

			// setters
			inline void set_reset(const bool reset) { m_reset = boolmask<u8>(reset); }

			inline void s1_w(const bool s1)	 // S1 pin
			{
				m_divider = (m_divider & 2) | (s1 ? 1 : 0);
			}

			inline void s2_w(const bool s2)	 // S2 pin
			{
				m_divider = (m_divider & 1) | (s2 ? 2 : 0);
			}

			inline void vck_w(const bool vck)  // VCK pin
			{
				set_vck(vck);
			}

			inline void set_4b_3b(const bool bit) { m_4b_3b = boolmask<u8>(bit); }

			inline void set_input(const u8 input)
			{
				m_input = m_4b_3b ? (input & 0xf) : (input & 0xe);
			}

			// getters
			inline bool vck() const { return m_vck; }

			// internal state
			virtual void reset() override;
			virtual s32 tick();

			template<typename T>
			void tick_stream(const std::size_t stream_len, T *out)
			{
				for (std::size_t s = 0; s < stream_len; s++)
				{
					out[s] = tick();
				};
			}

			inline s32 out() const { return m_out; }  // built in 10/12 bit DAC

			// for preview
			inline void voice_mute(const bool mute) { m_voice.set_mute(mute); }

			inline s32 voice_out() const { return m_voice.out(); }

		protected:
			// constructor
			msm5205_core(std::string tag, msm5205_intf &intf)
				: vox_core(tag)
				, m_voice(*this)
				, m_intf(intf)
				, m_vck(0)
				, m_divider(0)
				, m_reset(1)
				, m_input(0)
				, m_4b_3b(1)
				, m_reset_temp(1)
				, m_counter(0)
				, m_vck_counter(0)
				, m_out(0)
				, m_out_temp(0)
			{
			}

			// virtual functions
			virtual inline u8 divider() const { return m_msm5205_divider_table[divider()]; }

			// setters
			inline void set_vck(bool vck)
			{
				if (m_vck != vck)
				{
					m_intf.vck_state(m_vck);
					m_counter	  = 0;
					m_vck_counter = 0;
				}
			}

			inline void set_reset_temp(bool reset) { m_reset_temp = reset; }

			inline void set_counter(const u8 counter) { m_counter = counter & 0x3f; }

			inline void set_vck_counter(const u8 counter) { m_vck_counter = counter; }

			inline void set_output(const u8 out) { m_out = out; }

			inline void set_output_temp(const u8 out) { m_out_temp = out; }

			// getters
			inline bool reset_pin() const { return m_reset; }

			inline u8 input() const { return m_input; }

			inline bool reset_temp() const { return m_reset_temp; }

			inline u8 counter() const { return m_counter; }

			inline u8 vck_counter() const { return m_vck_counter; }

			inline s32 output() const { return m_out; }

			inline s32 output_temp() const { return m_out_temp; }

			voice_t &voice() { return m_voice; }

		private:
			// setters
			void vck_tick() {}

			// getters

			voice_t m_voice;
			msm5205_intf &m_intf;  // common memory interface

			u8 m_vck		: 1;   // VCK pin
			u8 m_divider	: 2;   // S1, S2 pin
			u8 m_reset		: 1;   // RESET pin
			u8 m_input		: 4;   // ADPCM input
			u8 m_4b_3b		: 1;   // 4bit/3bit ADPCM flag
			u8 m_reset_temp : 1;   // reset state
			u8 m_counter	: 6;   // another clock counter
			u8 m_vck_counter = 0;  // VCK counter
			s32 m_out		 = 0;  // 10 bit output
			s32 m_out_temp	 = 0;  // temporary buffer of above
	};

	class msm6585_core : public msm5205_core
	{
			friend class msm5205_intf;	// common memory interface

		private:
			// internal divider table (* 20)
			const u8 m_msm6585_divider_table[4] = {8, 4, 2, 1};

		public:
			// constructor
			msm6585_core(msm5205_intf &intf)
				: msm5205_core("msm6585", intf)
				, m_first_sample(true)
			{
			}

			// internal state
			virtual void reset() override;
			virtual s32 tick() override;

		protected:
			// virtual functions
			virtual inline u8 divider() const override
			{
				return m_msm6585_divider_table[divider()];
			}

		private:
			bool m_first_sample = true;
	};
}  // namespace vgsound_emu

#endif
