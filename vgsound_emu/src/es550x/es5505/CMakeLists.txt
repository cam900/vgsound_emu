#
# License: Zlib
# see https://gitlab.com/cam900/vgsound_emu/-/blob/main/LICENSE for more details
#
# Copyright holder(s): cam900
# CMake for vgsound_emu
#
project(vgsound_emu_es5505
	VERSION ${VGSOUND_EMU_VERSION}
	LANGUAGES CXX)

enable_language(CXX)

if(${CMAKE_VERSION} VERSION_GREATER "3.1.0" OR ${CMAKE_VERSION} VERSION_EQUAL "3.1.0")
	set(CMAKE_CXX_STANDARD ${VGSOUND_EMU_CXX_STANDARD})
	set(CMAKE_CXX_STANDARD_REQUIRED ON)
	set(CMAKE_CXX_EXTENSIONS OFF)
endif()

set(SOURCE_LIST "")

# ES5505
list(APPEND SOURCE_LIST
	es5505.hpp
	es5505.cpp
)
message(STATUS "Using ES5505 core")

add_library(${PROJECT_NAME} STATIC ${SOURCE_LIST})
add_library(vgsound_emu::es5505 ALIAS ${PROJECT_NAME})
target_compile_options(${PROJECT_NAME} PRIVATE ${VGSOUND_EMU_CXX_FLAGS})

if(VGSOUND_EMU_SHARED)
	set_property(TARGET ${PROJECT_NAME} PROPERTY POSITION_INDEPENDENT_CODE ON)
endif()
