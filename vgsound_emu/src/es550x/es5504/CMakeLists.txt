#
# License: Zlib
# see https://gitlab.com/cam900/vgsound_emu/-/blob/main/LICENSE for more details
#
# Copyright holder(s): cam900
# CMake for vgsound_emu
#
project(vgsound_emu_es5504
	VERSION ${VGSOUND_EMU_VERSION}
	LANGUAGES CXX)

enable_language(CXX)

if(${CMAKE_VERSION} VERSION_GREATER "3.1.0" OR ${CMAKE_VERSION} VERSION_EQUAL "3.1.0")
	set(CMAKE_CXX_STANDARD ${VGSOUND_EMU_CXX_STANDARD})
	set(CMAKE_CXX_STANDARD_REQUIRED ON)
	set(CMAKE_CXX_EXTENSIONS OFF)
endif()

set(SOURCE_LIST "")

# ES5504
list(APPEND SOURCE_LIST
	es5504.hpp
	es5504.cpp
)
message(STATUS "Using ES5504 core")

add_library(${PROJECT_NAME} STATIC ${SOURCE_LIST})
add_library(vgsound_emu::es5504 ALIAS ${PROJECT_NAME})
target_compile_options(${PROJECT_NAME} PRIVATE ${VGSOUND_EMU_CXX_FLAGS})

if(VGSOUND_EMU_SHARED)
	set_property(TARGET ${PROJECT_NAME} PROPERTY POSITION_INDEPENDENT_CODE ON)
endif()
