# NDS

## Summary

- 16 voice IMA ADPCM or PCM
  - voice 8 to 13 can be switched to PSG
  - voice 14 to 15 can be switched to Noise
  - accessible in main RAM
- 2 Sound capture unit
- 10 bit stereo output with bias

## Source code

- nds.hpp: Base header
  - nds.cpp: Source emulation core

## Description

It's Nintendo's handheld console with dual screen (bottom is touch screen), stylus pen, GBA compatibility and 16 hardware sound channels.

It's hardware sound channels are can be playable IMA ADPCM, 8/16 bit PCM and PSG, Noise (at last 8 channels).

## TODO

- Needs to further verifications from real hardware
- Hardware bug not implemented

## Register layout

```
Address Bits                                    R/W Description
        1111 1111 1111 1111 0000 0000 0000 0000
        fedc ba98 7654 3210 fedc ba98 7654 3210

000...00f Voiee 0 Register

000     x--- ---- ---- ---- ---- ---- ---- ---- R/W Busy
        -xx- ---- ---- ---- ---- ---- ---- ---- R/W Format
        -00- ---- ---- ---- ---- ---- ---- ----     8 bit linear PCM
        -01- ---- ---- ---- ---- ---- ---- ----     16 bit linear PCM
        -10- ---- ---- ---- ---- ---- ---- ----     IMA ADPCM
        -11- ---- ---- ---- ---- ---- ---- ----     PSG (voice 8 to 13) or Noise (voice 14 to 15)
        ---x x--- ---- ---- ---- ---- ---- ---- R/W Repeat mode
        ---0 0--- ---- ---- ---- ---- ---- ----     Off
        ---0 1--- ---- ---- ---- ---- ---- ----     Manual
        ---1 0--- ---- ---- ---- ---- ---- ----     Loop infinitely
        ---1 1--- ---- ---- ---- ---- ---- ----     One-shot
        ---- -xxx ---- ---- ---- ---- ---- ---- R/W Pulse duty (12.5% per step)
        ---- ---- -xxx xxxx ---- ---- ---- ---- R/W Panning*1
        ---- ---- ---- ---- x--- ---- ---- ---- R/W Hold
        ---- ---- ---- ---- ---- --xx ---- ---- R/W Volume multipler
        ---- ---- ---- ---- ---- --00 ---- ----     100%
        ---- ---- ---- ---- ---- --01 ---- ----     50%
        ---- ---- ---- ---- ---- --10 ---- ----     25%
        ---- ---- ---- ---- ---- --11 ---- ----     6.25%
        ---- ---- ---- ---- ---- ---- -xxx xxxx R/W Master volume

004     ---- -xxx xxxx xxxx xxxx xxxx xxxx xxxx   W Source address
008     xxxx xxxx xxxx xxxx ---- ---- ---- ----   W Loop start length (Word)
        ---- ---- ---- ---- xxxx xxxx xxxx xxxx   W Pitch
00c     ---- ---- --xx xxxx xxxx xxxx xxxx xxxx   W Length (Word)

010...01f Voiee 1 Register
020...02f Voiee 2 Register
...
0f0...0ff Voice 15 Register

100...107 Global Register

100     ---- ---- ---- ---- x--- ---- ---- ---- R/W Sound enable
        ---- ---- ---- ---- --x- ---- ---- ---- R/W Mix voice 3 output
        ---- ---- ---- ---- ---x ---- ---- ---- R/W Mix voice 1 output
        ---- ---- ---- ---- ---- xx-- ---- ---- R/W Right output source
        ---- ---- ---- ---- ---- 00-- ---- ----     Mixer
        ---- ---- ---- ---- ---- 01-- ---- ----     Voice 1 output
        ---- ---- ---- ---- ---- 10-- ---- ----     Voice 3 output
        ---- ---- ---- ---- ---- 11-- ---- ----     Voice 1 output + Voice 3 output
        ---- ---- ---- ---- ---- --xx ---- ---- R/W Left output source
        ---- ---- ---- ---- ---- --00 ---- ----     Mixer
        ---- ---- ---- ---- ---- --01 ---- ----     Voice 1 output
        ---- ---- ---- ---- ---- --10 ---- ----     Voice 3 output
        ---- ---- ---- ---- ---- --11 ---- ----     Voice 1 output + Voice 3 output
        ---- ---- ---- ---- ---- ---- -xxx xxxx R/W Master volume
104     ---- ---- ---- ---- ---- --xx xxxx xxxx R/W Output bias

108...11f Sound capture Register

108     ---- ---- ---- ---- xxxx xxxx ---- ---- R/W Sound capture 1 Control
        ---- ---- ---- ---- x--- ---- ---- ----     Busy
        ---- ---- ---- ---- ---- x--- ---- ----     Format
        ---- ---- ---- ---- ---- 0--- ---- ----     16 bit linear PCM
        ---- ---- ---- ---- ---- 1--- ---- ----     8 bit linear PCM
        ---- ---- ---- ---- ---- -x-- ---- ----     Repeat mode
        ---- ---- ---- ---- ---- -0-- ---- ----     Loop
        ---- ---- ---- ---- ---- -1-- ---- ----     One-shot
        ---- ---- ---- ---- ---- --x- ---- ----     Capture source
        ---- ---- ---- ---- ---- --0- ---- ----     Left(Capture 0) / Right(Capture 1) mixer
        ---- ---- ---- ---- ---- --1- ---- ----     Voice 0 (Capture 0) / Voice 2 (Capture 1); Bugged
        ---- ---- ---- ---- ---- ---x ---- ----     Output voice 1 (Capture 0) / voice 3 (Capture 1)
        ---- ---- ---- ---- ---- ---0 ---- ----     As such
        ---- ---- ---- ---- ---- ---1 ---- ----     Add to voice 0 (Capture 0) / voice 1 (Capture 1)
        ---- ---- ---- ---- ---- ---- xxxx xxxx R/W Sound capture 0 control

110...117 Capture 0 position/length register

110     ---- -xxx xxxx xxxx xxxx xxxx xxxx xxxx R/W Destination address
114     ---- ---- ---- ---- xxxx xxxx xxxx xxxx   W Buffer length (Word)

118...11f Capture 1 position/length register

*1 0 = Left speaker only, 0x40 = 50% at both speakers, 0x7f = Right speaker only
```

## Frequency calculation

```
 Frequency: Input clock / 2 / (65536 - Pitch)
```

## Reference

[https://problemkaputt.de/gbatek.htm]()
